<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    public $fillable = [
        'money'
    ];

    public function transfers() {
        return $this->hasMany(Transfer::class, 'wallet_id');
    }
}
