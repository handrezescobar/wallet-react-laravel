<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    public $fillable = [
        'description',
        'amount',
        'wallet_id'
    ];

    public function wallet() {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }
}
