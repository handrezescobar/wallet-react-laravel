<?php

namespace App\Http\Controllers;

use App\Transfer;
use App\Wallet;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    public function store(Request $request) {
        $wallet = Wallet::find($request->wallet_id);
        $wallet->money += $request->amount;
        $wallet->save();

        $transfer = Transfer::create([
            'description' => $request->description,
            'amount' => $request->amount,
            'wallet_id' => $request->wallet_id,
        ]);

        return response()->json($transfer, 201);
    }
}
